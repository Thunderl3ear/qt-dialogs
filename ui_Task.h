/********************************************************************************
** Form generated from reading UI file 'Task.ui'
**
** Created by: Qt User Interface Compiler version 6.2.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TASK_H
#define UI_TASK_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Task
{
public:
    QHBoxLayout *horizontalLayout;
    QGridLayout *gridLayout;
    QPushButton *editButton;
    QPushButton *assignButton;
    QPushButton *deadlineButton;
    QSpacerItem *horizontalSpacer;
    QPushButton *removeButton;
    QLineEdit *lineEdit;
    QCheckBox *checkbox;

    void setupUi(QWidget *Task)
    {
        if (Task->objectName().isEmpty())
            Task->setObjectName(QString::fromUtf8("Task"));
        Task->resize(400, 60);
        horizontalLayout = new QHBoxLayout(Task);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        editButton = new QPushButton(Task);
        editButton->setObjectName(QString::fromUtf8("editButton"));

        gridLayout->addWidget(editButton, 0, 1, 1, 1);

        assignButton = new QPushButton(Task);
        assignButton->setObjectName(QString::fromUtf8("assignButton"));

        gridLayout->addWidget(assignButton, 0, 3, 1, 1);

        deadlineButton = new QPushButton(Task);
        deadlineButton->setObjectName(QString::fromUtf8("deadlineButton"));

        gridLayout->addWidget(deadlineButton, 0, 4, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 0, 2, 1, 1);

        removeButton = new QPushButton(Task);
        removeButton->setObjectName(QString::fromUtf8("removeButton"));

        gridLayout->addWidget(removeButton, 0, 5, 1, 1);

        lineEdit = new QLineEdit(Task);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        gridLayout->addWidget(lineEdit, 1, 1, 1, 5);

        checkbox = new QCheckBox(Task);
        checkbox->setObjectName(QString::fromUtf8("checkbox"));

        gridLayout->addWidget(checkbox, 0, 0, 1, 1);


        horizontalLayout->addLayout(gridLayout);


        retranslateUi(Task);

        QMetaObject::connectSlotsByName(Task);
    } // setupUi

    void retranslateUi(QWidget *Task)
    {
        Task->setWindowTitle(QCoreApplication::translate("Task", "Form", nullptr));
        editButton->setText(QCoreApplication::translate("Task", "Edit", nullptr));
        assignButton->setText(QCoreApplication::translate("Task", "Unassigned", nullptr));
        deadlineButton->setText(QCoreApplication::translate("Task", "No Deadline", nullptr));
        removeButton->setText(QCoreApplication::translate("Task", "Remove", nullptr));
        lineEdit->setText(QCoreApplication::translate("Task", "Description", nullptr));
        checkbox->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class Task: public Ui_Task {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TASK_H
