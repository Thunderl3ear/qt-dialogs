#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QDebug>
#include <QInputDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    mTasks()
{
    ui->setupUi(this);

    set_style();
    connect(ui->addTaskButton, &QPushButton::clicked, this, &MainWindow::addTask);
    updateStatus();
}

void MainWindow::set_style()
{
    this->setStyleSheet("QWidget{"
                            "color: #5DADE2;"
                            "background-color: #424949;"
                            "font: bold 14px;"
                            "font-family: mononoki;"
                        "}"
                        "QPushButton{"
                            "color: #424949;"
                            "background-color: #5DADE2;"
                            "border-style: outset;"
                            "border-width: 0px;"
                            "border-radius: 10px;"
                            "min-width: 5em;"
                            "padding: 6px;"
                        "}"
                        "QLineEdit{"
                            "color: #424949;"
                            "background-color: #5DADE2;"
                            "border-style: outset;"
                            "border-width: 0px;"
                            "border-radius: 10px;"
                            "min-width: 5em;"
                            "padding: 6px;"
                        "}"
                        "QLabel{"
                            "color: #424949;"
                            "background-color: #58D68D;"
                            "border-style: outset;"
                            "border-width: 0px;"
                            "border-radius: 10px;"
                            "padding: 6px;"
                        "}"
                        "QCheckBox{"
                            "background-color: #E74C3C;"
                            "border-style: outset;"
                            "border-width: 0px;"
                            "border-radius: 10px;"
                            "padding: 6px;"
                        "}"
                        "QCheckBox::indicator:unchecked{"
                            "background-color: #E74C3C;"
                        "}"
                        "QCheckBox::checked{"
                            "background-color: #58D68D;"
                        "}"
                        "QCheckBox::indicator:checked{"
                            "background-color: #58D68D;"
                        "}"
                        "QPushButton::hover{"
                            "background-color: #58D68D;"
                        "}"
                        "QPushButton#removeButton::hover{"
                            "background-color: #E74C3C;"
                        "}"
                        "QLineEdit::hover{"
                            "background-color: #58D68D;"
                        "}");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addTask()
{
    bool ok;

    QString name = QInputDialog::getText(this, tr("Add task"),
                                         tr("Task name"), QLineEdit::Normal,
                                         tr("Untitled task"), &ok, Qt::FramelessWindowHint);
    if (ok && !name.isEmpty()) {
        qDebug() << "Adding new task";
        Task* task = new Task(name);
        connect(task, &Task::removed, this, &MainWindow::removeTask);
        connect(task, &Task::statusChanged, this, &MainWindow::taskStatusChanged);
        mTasks.append(task);
        ui->tasksLayout->addWidget(task);
        updateStatus();
    }
}

void MainWindow::removeTask(Task* task)
{
    mTasks.removeOne(task);
    ui->tasksLayout->removeWidget(task);
    delete task;
    updateStatus();
}

void MainWindow::taskStatusChanged(Task* /*task*/)
{
    updateStatus();
}

void MainWindow::updateStatus()
{
    int completedCount = 0;
    for(auto t : mTasks)  {
        if (t->isCompleted()) {
            completedCount++;
        }
    }
    int todoCount = mTasks.size() - completedCount;

    ui->statusLabel->setText(QString("Status: %1 todo / %2 completed")
                             .arg(todoCount)
                             .arg(completedCount));
    if(todoCount>0)
    {
        ui->statusLabel->setStyleSheet("background-color: #E74C3C;");
    }
    else
    {
        ui->statusLabel->setStyleSheet("background-color: #58D68D;");
    }
}
