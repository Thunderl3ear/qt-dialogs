#include "Task.h"
#include "ui_Task.h"

#include <QInputDialog>
#include <QDebug>
#include <QMessageBox>

Task::Task(const QString& name, QWidget *parent) :
        QWidget(parent),
        ui(new Ui::Task)
{
    ui->setupUi(this);
    setName(name);

    connect(ui->editButton, &QPushButton::clicked, this, &Task::rename);
    connect(ui->deadlineButton, &QPushButton::clicked, this, &Task::setDeadline);
    connect(ui->assignButton, &QPushButton::clicked, this, &Task::setAssignee);
    connect(ui->removeButton, &QPushButton::clicked, this, &Task::confirmDelete);


    connect(ui->checkbox, &QCheckBox::toggled, this, &Task::checked);
}


void Task::confirmDelete()
{
    QMessageBox msgBox(this->parentWidget());
    msgBox.setText("Are you sure you want to delete this task?");
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::No);
    msgBox.setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
    int ret = msgBox.exec();
    switch (ret) {
      case QMessageBox::Yes:
          emit removed(this);
          break;
      default:
          break;
    }
}

Task::~Task()
{
    qDebug() << "~Task() called";
    delete ui;
}

void Task::setName(const QString& name)
{
    ui->editButton->setText(name);
}

QString Task::name() const
{
    return ui->editButton->text();
}

bool Task::isCompleted() const
{
   return ui->checkbox->isChecked();
}

void Task::setDeadline()
{
    bool ok;
    QString value = QInputDialog::getText(this, tr("Edit deadline"),
                                          tr("Deadline"), QLineEdit::Normal,
                                          "", &ok, Qt::FramelessWindowHint);
    if (ok && !value.isEmpty()) {
        ui->deadlineButton->setText(value);
    }
}

void Task::setAssignee()
{
    bool ok;
    QString value = QInputDialog::getText(this, tr("Edit asssignee"),
                                          tr("Assignee"), QLineEdit::Normal,
                                          ui->assignButton->text(), &ok, Qt::FramelessWindowHint);
    if (ok && !value.isEmpty()) {
        ui->assignButton->setText(value);
    }
}

void Task::rename()
{
    bool ok;
    QString value = QInputDialog::getText(this, tr("Edit task"),
                                          tr("Task name"), QLineEdit::Normal,
                                          this->name(), &ok, Qt::FramelessWindowHint);
    if (ok && !value.isEmpty()) {
        setName(value);
    }
}

void Task::checked(bool checked)
{
    QFont font(ui->checkbox->font());
    font.setStrikeOut(checked);
    ui->checkbox->setFont(font);

    emit statusChanged(this);
}
