#include "MainWindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}

/*# Extend the project https://gitlab.com/plczapiewski/qt_todo_list
with at least one of the following feature:
- add task description;
- add task deadline;
- add task assignee;
- add option to customize the font used in the application (use font Dialog);
- add translation to your native language;
- other feature? propose meaningful extension for the
*/
