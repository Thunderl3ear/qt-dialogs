# Beautiful ToDo list

## Table of Contents

- [Requirements](#requirements)
- [Setup](#setup)
- [About](#about)
- [Maintainers](#maintainers)
- [Contributers](#contributers)

## Requirements

Requires `Qt Creator 6.2.3`.

## Setup
* Build and Run through QT Creator

## About
Modified version of the ToDo list. Focused on creating a modern-style interface.

## Maintainers

[Thorbjørn (@Thunderl3ear)](https://gitlab.com/Thunderl3ear)

## Contributers and License
Copyright 2022,

[Thorbjørn (@Thunderl3ear)](https://gitlab.com/Thunderl3ear)

